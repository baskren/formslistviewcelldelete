﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CellDelete
{
	public class App : Application
	{
		public class Item : BindableObject {
		}

		public class Item<T> : Item {
			public static BindableProperty ValueProperty = BindableProperty.Create ("Value", typeof(T), typeof(Item<T>), default(T));
			public T Value {
				get { return (T)GetValue (ValueProperty); }
				set { SetValue (ValueProperty, value); }
			}
		}

		public class ItemGroup : ObservableCollection<Item> {
			public string Title;
		}

		public class HeaderItem : Item<string> {
		}

		ObservableCollection<Item> Items;
		ListView listView;

		public App()
		{
			Items = new ObservableCollection<Item> ();

			Items.Add(new HeaderItem() { Value = "Strings:" });
			for (int i = 0; i < 10; i++)
				Items.Add (new Item<string> () { Value = "strings[" + i + "]"} );

			Items.Add(new HeaderItem() { Value = "Booleans:" });
			for (int i = 0; i < 10; i++)
				Items.Add (new Item<bool> () { Value = i%2==1 });

			listView = new ListView () {
				ItemsSource = Items,
				//ItemTemplate = new DataTemplate (typeof(MyCell)),
				ItemTemplate = new Selector(),
				IsGroupingEnabled = false,
				HasUnevenRows = true,
			};
			listView.ItemSelected += OnItemSelected;

			listView.IsRefreshing = false;

			MainPage = new ContentPage () {
				Content = listView,
				Padding = new Thickness(0,20,0,0),
			};
		}

		Item lastItem = new Item<string>() { Value = "pizza" };
		public void OnItemSelected(object sender, SelectedItemChangedEventArgs e) {
			listView.SelectedItem = null;
			var item = e.SelectedItem as Item;
				if (Items.Contains (item)) {
					int index = Items.IndexOf (item);
					var tmpItem = item;
					Items.Remove (item);
					Items.Insert (index, lastItem);
					System.Diagnostics.Debug.WriteLine ("removed:" + item + " for:" + lastItem);
					lastItem = tmpItem;
					return;
				}
		}

		public class Selector : DataTemplateSelector {
			public DataTemplate headerTemplate = new DataTemplate (typeof(HeaderCell));
			public DataTemplate stringTemplate = new DataTemplate (typeof(StringCell));
			public DataTemplate boolTemplate = new DataTemplate (typeof(BoolCell));

			protected override DataTemplate OnSelectTemplate (object item, BindableObject container)
			{
				var type = item.GetType ();
				if (type == typeof(HeaderItem))
					return headerTemplate;
				if (type == typeof(Item<string>))
					return stringTemplate;
				if (type == typeof(Item<bool>))
					return boolTemplate;
				return null;
			}
		}

		public class HeaderCell : ViewCell {
			public HeaderCell() {
				View = new Label { TextColor = Color.White, BackgroundColor = Color.Blue, HeightRequest=25 };
			}
			protected override void OnBindingContextChanged() {
				base.OnBindingContextChanged ();
				((Label)View).Text = ((HeaderItem)BindingContext).Value;
			}
		}


		public class StringCell : ViewCell {
			Label _label;

			public StringCell() {
				_label = new Label();
				_label.SetBinding(Label.TextProperty,"Value");
				_label.HeightRequest = 50;
				_label.BackgroundColor = Color.White;
				View = _label;
			}
		}

		public class BoolCell: ViewCell {
			Switch _switch;

			public BoolCell() {
				_switch = new Switch();
				_switch.SetBinding(Switch.IsToggledProperty,"Value");
				_switch.HeightRequest = 50;
				_switch.BackgroundColor = Color.Gray;
				View = _switch;
			}
		}

		/*
		public class MyCell : ViewCell {
			
			Label _label;
			Switch _switch;
			public MyCell() {
				_label = new Label();
				_label.SetBinding(Label.TextProperty,"Value");
				_switch = new Switch();
				_switch.SetBinding(Switch.IsToggledProperty,"Value");
				View = new StackLayout() {
					Children = { _label, _switch },
					Orientation = StackOrientation.Vertical,
					HeightRequest = 56,
				};
			}

			protected override void OnBindingContextChanged() {
				base.OnBindingContextChanged ();

				View.BindingContext = BindingContext;
				Type type = BindingContext?.GetType ();
				_label.IsVisible = false;
				_switch.IsVisible = false;
				if (type == typeof(Item<string>) || type == typeof(HeaderItem))
					_label.IsVisible = true;
				else if (type == typeof(Item<bool>))
					_switch.IsVisible = true;
				if (type == typeof(HeaderItem)) {
					View.BackgroundColor = Color.Gray;
					_label.TextColor = Color.White;
					View.HeightRequest = 30;
				} else {
					View.BackgroundColor = Color.White;
					_label.TextColor = Color.Black;
					View.HeightRequest = 56;
				}
			}
		}
		*/
	}
}
